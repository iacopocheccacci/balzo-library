/****************************************************************************
Copyright (c) 2008-2010 Ricardo Quesada
Copyright (c) 2010-2012 cocos2d-x.org
Copyright (c) 2011      Zynga Inc.
Copyright (c) 2013-2014 Chukong Technologies Inc.
 
http://www.cocos2d-x.org

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
****************************************************************************/
package eu.balzo.aliengarden;

import java.util.ArrayList;
import java.util.List;
import java.util.TimeZone;

import org.cocos2dx.lib.Cocos2dxActivity;

import android.os.Bundle;
import android.util.Log;

//se uso AnalyticXBridge per Flurry ( ovvero non uso Plugin-X )
//import com.diwublog.AnalyticX.AnalyticXBridge;

import android.app.Activity;
import android.content.Intent;
import android.content.pm.PackageManager.NameNotFoundException;
import android.graphics.Rect;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.Bundle;
import android.telephony.SignalStrength;
import android.test.IsolatedContext;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup.LayoutParams;
import android.widget.LinearLayout;

import com.google.android.gms.ads.*;
import com.screw.facebook.Facebook;
import com.chartboost.sdk.*;
import com.facebook.*;
import com.facebook.model.*;

import org.cocos2dx.plugin.PluginWrapper;
import org.cocos2dx.lib.Cocos2dxGLSurfaceView;

import android.os.AsyncTask;    

import com.google.android.gms.games.Games;
import com.google.android.gms.gcm.*;
import com.microsoft.windowsazure.messaging.*;
import com.microsoft.windowsazure.notifications.NotificationsManager;

import com.avalon.*;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.games.Games;
import com.google.example.games.basegameutils.GameHelper;
import com.google.example.games.basegameutils.GameHelper.GameHelperListener;
import com.google.example.games.basegameutils.GameHelper.SignInFailureReason;

import eu.balzo.aliengarden.util.*;

//import org.fmod.FMODAudioDevice;

public class AppActivity extends Cocos2dxActivity {

	//costante definita vera se l'apk va pubblicata senza banner su uno dei market
	//utilizzati dal publisher 360.
	
	private static final boolean CHINA_MARKET = false;
	private static final boolean NO_BANNER = false;
	private static final boolean NO_INTERSTITIAL = false;
	
    public Cocos2dxGLSurfaceView onCreateView() {
        Cocos2dxGLSurfaceView glSurfaceView = new Cocos2dxGLSurfaceView(this);
        // TestCpp should create stencil buffer
        glSurfaceView.setEGLConfigChooser(5, 6, 5, 0, 16, 8);
        
        PluginWrapper.init(this);
        PluginWrapper.setGLSurfaceView(glSurfaceView);
        return glSurfaceView;
    }
    
	static AdView adView;
	static LinearLayout admobBannerContainer;
	static InterstitialAd interstitial;
	static Activity me;
	//put here your id
	//final String ADMOB_ID="a1536ba5cc5433c";
	//AdMob id per non China
	private static String AD_UNIT_ID;

	 
	final String CHARTBOOST_APP_ID = "5368a9cfc26ee424e87615fe";
	final String CHARBOOST_APP_SIGNATURE = "fa7b7c750f016d6d242e62bbdae5d7142eff7004";

	private static Chartboost cb;
	
	//Windows Azure 
	//private String SENDER_ID = "shaped-pride-627";
	private String SENDER_ID = "598066530137";
	private GoogleCloudMessaging gcm;
	private NotificationHub hub;
	
	private static GameHelper mGameHelper;
    private final static int REQUEST_ACHIEVEMENTS = 9003;
    private final static int REQUEST_LEADERBOARD = 9004;
    private static boolean mShowLeaderboard;
    private static int mScoreToBePosted;
    private static String mLeaderBoardId;
    
    //Google Play IAP
    private static IabHelper mHelper;
    IabHelper.QueryInventoryFinishedListener mQueryFinishedListener;
    boolean iabSetupFailed;
    private static IabHelper.OnIabPurchaseFinishedListener mPurchaseFinishedListener;
    private static boolean mHasRemovedAds;
    private static boolean mPurchasingAds;
    private static boolean mUserCancelledSignIn;
    private static int mDaysSinceInstall;

	@Override
    protected void onCreate(Bundle savedInstanceState){
		super.onCreate(savedInstanceState);
		me=this;
		
		mPurchasingAds = false;
		
		if(!CHINA_MARKET)
			AD_UNIT_ID = "ca-app-pub-6602706598360211/4961398685";
		else
		{
			if(NO_BANNER)
				AD_UNIT_ID = "ca-app-pub-6602706598360211/6056781481";
			else
				AD_UNIT_ID = "ca-app-pub-6602706598360211/4580048283";
		}
		//se uso AnalyticXBridge per Flurry ( ovvero non uso Plugin-X )
		//AnalyticXBridge.sessionContext = this.getApplicationContext();
		
		// se usiamo FMOD
		//FMODAudioDevice.init(this);
		
		//AdMob
		//adView = null;
  		try {
  			LayoutParams adParams = new LayoutParams(LayoutParams.MATCH_PARENT,LayoutParams.MATCH_PARENT);

  		    // Create an ad.
  		    adView = new AdView(this);
  		    adView.setAdSize(AdSize.BANNER);
  		    adView.setAdUnitId(AD_UNIT_ID);
  		    
  		    admobBannerContainer = new LinearLayout(this);
  		    admobBannerContainer.setGravity(Gravity.BOTTOM);
  		    admobBannerContainer.addView(adView);
  		    //this.addContentView(admobBannerContainer, new LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.MATCH_PARENT));
  		    
  		    //adView.setVisibility(View.INVISIBLE);
  		    
//  			adView = new AdView(this, AdSize.BANNER,
//  					ADMOB_ID);			
//  			//adView.setAdListener(this);
//  			AdRequest request = new AdRequest();
//  			adView.loadAd(request);
//  			adView.setGravity(Gravity.BOTTOM|Gravity.CENTER);
  			// Adding full screen container
  			//
  	       //addContentView(adView, adParams);
  			
  		    // Create an ad request. Check logcat output for the hashed device ID to
  		    // get test ads on a physical device.
 			
  		    
  		} catch (Exception e) {
  			Log.d("", "error: " + e);
  		}
  		
  		//sul mercato non google play cinese gli interstitial sono di admob
  		//invece che chartboost
		if(CHINA_MARKET)
		{
	  	    // Create the Admob interstitial.
	  	    interstitial = new InterstitialAd(this);
	  	    interstitial.setAdUnitId(AD_UNIT_ID);

	  	    // Create ad request.
	  	    AdRequest adRequest = new AdRequest.Builder().build();

	  	    // Begin loading your interstitial.
	  	    interstitial.loadAd(adRequest);
			return;

		}
  	    
  		// Configure Chartboost
  		cb = Chartboost.sharedChartboost();
  		String appId = CHARTBOOST_APP_ID;
  		String appSignature = CHARBOOST_APP_SIGNATURE;
  		cb.onCreate(this, appId, appSignature, null);
  		
  		CBPreferences.getInstance().setImpressionsUseActivities(true);
  		
  		//cb.cacheInterstitial();
		
		//Microsoft Azure - Notification Hub
		NotificationsManager.handleNotifications(this, SENDER_ID, MyHandler.class);

		gcm = GoogleCloudMessaging.getInstance(this);

		String connectionString = "Endpoint=sb://hubaliengarden-ns.servicebus.windows.net/;SharedAccessKeyName=DefaultListenSharedAccessSignature;SharedAccessKey=32eF4Gkg8zkyRKqvZymyQbhmGGpHzBWxJ190hiOtOxY=";
		hub = new NotificationHub("hubaliengarden", connectionString, this);

		registerWithNotificationHubs();
		
	    // create game helper with all APIs (Games, Plus, AppState):
	    mGameHelper = new GameHelper(this, GameHelper.CLIENT_GAMES);
	    mShowLeaderboard = false;
	    mScoreToBePosted = 0;
	    
	    mUserCancelledSignIn = false;
	    if(mGameHelper != null) {
	    			
	    	// enable debug logs (if applicable)
	    	mGameHelper.enableDebugLog(true, "GameHelper");

	    	GameHelperListener listener = new GameHelper.GameHelperListener() {
	    		@Override
	    		public void onSignInSucceeded() {
	    			// handle sign-in succeess
	    			if(mScoreToBePosted > 0)
	    				postScore(mLeaderBoardId, mScoreToBePosted);
	    			if(mShowLeaderboard)
	    				showScores();
	    			Log.v("GameHelper", "onSignInSuccess");
	    		}
	    		@Override
	    		public void onSignInFailed() {
	    			// handle sign-in failure (e.g. show Sign In button)
	    			Log.v("GameHelper", "onSignInFailed");
	    			SignInFailureReason failure = mGameHelper.getSignInError();
	    			
	    			//verificare che se e' fallito il signin ma e' nulla la signinfailurereason 
	    			//e' stato cancellato dall'utente
	    			//in quel caso inibire il signin fino a nuovo restart dell'applicazione
	    			if( failure == null)
	    			{
	    				mUserCancelledSignIn = true;
	    			}
	    		}

	    	};
	    	
	    	mGameHelper.setup(listener);
	    	mGameHelper.setConnectOnStart(false);

	    }

  		//com.chartboost.sdk.ChartboostActivity.this.
//  		
//  	    if(isARMv7())
//  	    	Log.d("", "Raffa: is armv7");		
  		
  		//screw per Facebook
  		Facebook.onActivityCreate(this, savedInstanceState);
  		
  	   //IAP Google Play
  		mHasRemovedAds = false;
  		String base64EncodedPublicKey =
  		"MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAt5lYQymzJ/AaPB2V61sbh0YPRX+R0VFxUAQWeDSwwbWzqKfkLAlJMcb4d6heuEFjRm1w4OExIm/MN0mU5LbsKd8BlWy3h9kpYmvTkmfoXM1U5GyXu9ylFpA2AqTn2uEgGM5on2AWH7lyinjYJMRQNnsT3M1eNSSOwO5MMefojj1xG/tQTQ/pw6aPG/Ml9jpcVAAxwEf6XksRJo3S6ezNM0ca3IO9bfu1PXdmyTTP4utPIwk+FEG8/AtMmCzBokNo/apSmrnD5X4Xj9EE5+sGU8GZKdP0PDf87L1hGcMa7WlhljkpVFsVH3PeQTt9tJnF3eewIzBWpVohffscaFxLfQIDAQAB";

  		iabSetupFailed = true;
  		// compute your public key and store it in base64EncodedPublicKey
  		mHelper = new IabHelper(this, base64EncodedPublicKey);
  		mHelper.enableDebugLogging(true);
  		mHelper.startSetup(new IabHelper.OnIabSetupFinishedListener() {
  			public void onIabSetupFinished(IabResult result) {
  				
  				if (!result.isSuccess()) {
  					// Oh noes, there was a problem.
  					Log.d("IabHelper", "Problem setting up In-app Billing: " + result);
  					return;
  				}            

  				if(mHelper == null) 
  					return;
  				// Hooray, IAB is fully set up!
  				iabSetupFailed = false;
  				List<String> additionalSkuList = new ArrayList<String>();
  				additionalSkuList.add("removeads");
  				mHelper.queryInventoryAsync(true, additionalSkuList,
  						mQueryFinishedListener);
  			}
  		});

  		//eseguo una query per verificare se il removeads e' gia' stato acquistato 
  		//in una precedente installazione.
  		//serve per rimuovere gli ads ad una nuova installazione
  		mQueryFinishedListener = new IabHelper.QueryInventoryFinishedListener() {
  			public void onQueryInventoryFinished(IabResult result, Inventory inventory)   
  			{

  				// Have we been disposed of in the meantime? If so, quit.
  				if (mHelper == null) return;

  				if (result.isFailure()) {
  					// handle error
  					return;
  				}

  				String price = inventory.getSkuDetails("removeads").getPrice();
  				Purchase removeAdsPurchase = inventory.getPurchase("removeads");
  				mHasRemovedAds = (removeAdsPurchase != null );
  				//mHasRemovedAds = false;
  				
  				if(mHasRemovedAds)
  				{
  					nativeOnRemoveAdsPaymentSuccessful();
  					hideAdmobJNI();
  				}
  			}
  		};

  		mPurchaseFinishedListener = new IabHelper.OnIabPurchaseFinishedListener() {
  			public void onIabPurchaseFinished(IabResult result, Purchase purchase) 
  			{
  				mPurchasingAds = false;
  				//nativeOnRemoveAdsPaymentSuccessful();
  				//mHasRemovedAds = true;
  				
  				if (result.isFailure()) {
  					Log.d("IabHelper", "Error purchasing: " + result);
  					return;
  				}      
  				
  				else if (result.isSuccess() && purchase.getSku().equals("removeads")) {
  					//update the UI and write on user default
  					nativeOnRemoveAdsPaymentSuccessful();
  					mHasRemovedAds = true;
  					
  				}
  			}
  		};

  		//Install date
  		try {
			long installed = getPackageManager()
				    .getPackageInfo("eu.balzo.aliengarden", 0)
				    .firstInstallTime
				;
			long millisSinceInstall = System.currentTimeMillis() - installed;
			mDaysSinceInstall = (int) (millisSinceInstall/86400000L);
		} catch (NameNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
  	}
	
	@Override
	protected void onResume()
	{
		super.onResume();

	    if (adView != null && !NO_BANNER) {
	        adView.resume();
	    }
	    
	    
		if(CHINA_MARKET)
			return;
		
		
		com.facebook.AppEventsLogger.activateApp(this, "281024655411962");
		Facebook.onActivityResume();
		
		//showAdmobJNI();
		

	    
	}
	
    @Override
    protected void onStart()
    {
        super.onStart();

        if(CHINA_MARKET)
        	return;
        
        if(!NO_INTERSTITIAL)
        {
	        cb.onStart(this);
	        //Notify the beginning of a user session. Must not be dependent on user actions or any prior network requests.
	        cb.startSession();
	        //cb.cacheMoreApps();
        }

        if(mGameHelper != null)
        {
        	mGameHelper.onStart(this);
//        	View popupview = new View(this);
//        	addContentView(popupview, new LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.MATCH_PARENT));
//        	Games.setViewForPopups(mGameHelper.getApiClient(), popupview);
        }
    }
    
    @Override
    protected void onPause()
    {
        if (adView != null && !NO_BANNER) {
            adView.pause();
          }
        
        if(CHINA_MARKET)
        {
        	super.onPause();
        	return;
        }
        
        Facebook.onActivityPause();
    	super.onPause();
    	
    }

    @Override
    protected void onStop()
    {
        super.onStop();
        
        if(CHINA_MARKET)
        	return;
        
        if(!NO_INTERSTITIAL)
        	cb.onStop(this);
        
        if(mGameHelper != null)
        	mGameHelper.onStop();
    }

    @Override
    protected void onDestroy()
    {
    	Log.d("Raffa","Raffa: onDestroy");
        if (adView != null && !NO_BANNER) {
            adView.destroy();
        }    	
    	//FMODAudioDevice.close();
        
        if(CHINA_MARKET)
        {
        	super.onDestroy();
        	return;
        }
        
        if(!NO_INTERSTITIAL)
        	cb.onDestroy(this);
        
    	if (mHelper != null) mHelper.dispose();
    	mHelper = null;
    	
        super.onDestroy();
    }

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		Log.d("AppActivity", "onActivityResult(" + requestCode + "," + resultCode + "," + data);
		
		if(CHINA_MARKET)
		{
			super.onActivityResult(requestCode, resultCode, data);
			return;
		}
		
        // Pass on the activity result to the helper for handling
        if (mHelper.handleActivityResult(requestCode, resultCode, data)) {
        	Log.d("AppActivity", "onActivityResult handled by IABUtil.");
        	return;
            
        }
		Facebook.onActivityResult(requestCode, resultCode, data);
		
		mGameHelper.onActivityResult(requestCode, resultCode, data);
		super.onActivityResult(requestCode, resultCode, data);
	}
	
    @Override
    public void onBackPressed()
    {
    	if(cb != null)
    		if(this.cb.onBackPressed())
    			return;
//        // If an interstitial is on screen, close it. Otherwise continue as normal.
//        if (this.cb.onBackPressed())
//            return;
//        else
//            super.onBackPressed();
    	super.onBackPressed();
    }	
    
    @SuppressWarnings("unchecked")
    private void registerWithNotificationHubs() {
       new AsyncTask() {
          @Override
          protected Object doInBackground(Object... params) {
             try {
                String regid = gcm.register(SENDER_ID);
                
                //creo il tag in base al fuso orario con il formato (sgn)[hh]
                int millis = TimeZone.getDefault().getRawOffset();
                int hours = millis/(3600000);
                String tag;
                if(hours >= 0)
                {
                	tag = String.format("%02d", hours);
                }
                else
                {
                	tag = String.format("%03d", hours);
                }
                
                hub.register(regid, tag);
                //hub.register(regid);
             } catch (Exception e) {
                return e;
             }
             return null;
         }
       }.execute(null, null, null);
    }
    
    
    static void openAppJNI() {
    	me.startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("market://search?q=pub:Balzo srl")));
    	//me.startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("http://play.google.com/store/apps/developer?id=Balzo+srl")));
    	
    }
    
    static void rateAppJNI() {
    	me.startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=eu.balzo.aliengarden")));
    	//me.startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("http://play.google.com/store/apps/developer?id=Balzo+srl")));
    	
    }
    
    static void showInterstitialJNI() {
    	Log.d("AppActivity:", "show interstitial");
    	
    	if(NO_INTERSTITIAL)
    		return;
    	
    	if(CHINA_MARKET)
    	{
    		me.runOnUiThread(new Runnable() {
    			@Override
    			public void run() {
    		        if (interstitial.isLoaded()) {
    		            interstitial.show();
    		        }
    		  	    // Create ad request.
    		  	    AdRequest adRequest = new AdRequest.Builder().build();

    		  	    // Begin loading your interstitial.
    		  	    interstitial.loadAd(adRequest);
    			}
    		});    	
    		return;
    	}
		
    	Log.d("AppActivity:", "cb interstitial");
		cb.showInterstitial();    	
    	
    }
    
    
    static void CBShowMoreApps() {
    	if(cb != null)
    		cb.showMoreApps();
    }
	
    static void showAdmobJNI(){
    	if(NO_BANNER)
    		return;
    	
		me.runOnUiThread(new Runnable() {
			@Override
			public void run() {
				
				if (adView != null) {
					AdRequest adRequest = new AdRequest.Builder()
					//  		        .addTestDevice(AdRequest.DEVICE_ID_EMULATOR)
					//  		        .addTestDevice("1B16B6F3AED7A48B4DB93E1EC96CB7F9") //mio GT9100 hash
					.build();

					// Start loading the ad in the background.
					adView.loadAd(adRequest); 
					Log.d("AppActivity:", "loading Ad request");
				}
			}
		});
	}

	static void hideAdmobJNI(){
		if(NO_BANNER)
			return;
		
		me.runOnUiThread(new Runnable() {
			@Override
			public void run() {
				if (adView != null && admobBannerContainer != null) {
					admobBannerContainer.removeAllViews();
				}
			}
		});
    }

    
    static int getBannerHeightJNI(){
    	
    	if(NO_BANNER)
    		return 0;
    	//return adView.getHeight();
    	return AdSize.BANNER.getHeightInPixels(me);
    }	
    
    static int daysSinceInstall() {
    	Log.d("AppActivity:", String.valueOf(mDaysSinceInstall));
    	return mDaysSinceInstall;
    }
    public static void signIn()
    {
    	if(mUserCancelledSignIn)
    		return;
    	
    	if(CHINA_MARKET)
    		return;
    	
    	if(mGameHelper == null)
    		return;
    	
        me.runOnUiThread(new Runnable() {
            public void run() {
            	
            	//controllare se e' presente connessione internet
            	ConnectivityManager cm = (ConnectivityManager) me.getSystemService(me.CONNECTIVITY_SERVICE);
            	NetworkInfo ni = cm.getActiveNetworkInfo();
            	
            	if (ni != null) 
            		  mGameHelper.beginUserInitiatedSignIn();
            }
        });
    }
    public static boolean isSignedIn()
    {
    	if(CHINA_MARKET)
    		return false;
    	
    	if(mGameHelper == null)
    		return false;
        return (mGameHelper != null && mGameHelper.isSignedIn());
    }    
    public static boolean showScores()
    {
    	if(CHINA_MARKET)
    		return false;
    	
    	//se voglio vedere la leaderboard, ma non sono registrato
    	//rilancio il processo di registrazione e sulla callback di avvenuta registrazione 
    	//rilancio la showScores
    	mUserCancelledSignIn = false;
        if (!isSignedIn()) {
        	mShowLeaderboard = true;
        	signIn();
            return false;
        }
        
        me.runOnUiThread(new Runnable() {
            public void run() {
                me.startActivityForResult(
                	Games.Leaderboards.getAllLeaderboardsIntent(mGameHelper.getApiClient()),
                    REQUEST_LEADERBOARD
                );
            }
        });
        return true;
    }    
    public static void postScore(final String idName, final int score)
    {
    	if(CHINA_MARKET)
    		return;
    	
        if (!isSignedIn()) {
        	mScoreToBePosted = score;
        	mLeaderBoardId = idName;
        	signIn();
            return;
        }

        
        me.runOnUiThread(new Runnable() {
            public void run() {
                int rId = getResourceId(idName);
                if (rId > 0) {
                    String googleId = me.getResources().getString(rId);
                    Games.Leaderboards.submitScore(mGameHelper.getApiClient(), googleId, score);
                }
            }
        });
    }
    private static int getResourceId(String idName)
    {
        int rId = me.getResources().getIdentifier(idName, "string", me.getPackageName());
        if (rId == 0) {
            Log.v("GameHelper", "Unable to find resource ID for string " + idName);
        }
        return rId;

    }
	 
    public static void purchaseRemoveAds()
    {
    	if(CHINA_MARKET)
    		return;
    	if(mPurchasingAds)
    		return;
    	
    	mPurchasingAds = true;
    	
    	if(mHelper!=null)
    		mHelper.launchPurchaseFlow(me, "removeads", 10001,   
    				mPurchaseFinishedListener, null);
    }
    
    public static boolean hasRemovedAds()
    {
    	if(CHINA_MARKET)
    		return false;
    	
    	return mHasRemovedAds;
    }
    public static boolean isChinaMarket()
    {
    	return CHINA_MARKET;
    }
    
    private static native void nativeOnRemoveAdsPaymentSuccessful(); 
/*
	static {

    	// Try debug libraries...
    	try { 
    		System.loadLibrary("fmodD");
    		System.loadLibrary("fmodstudioD"); }
    	catch (UnsatisfiedLinkError e) { }
    	// Try logging libraries...
    	try {
    		System.loadLibrary("fmodL");
    		System.loadLibrary("fmodstudioL"); }
    	catch (UnsatisfiedLinkError e) { }
		//Try release libraries...		
		try { 
			System.loadLibrary("fmod");
			System.loadLibrary("fmodstudio"); 
		}
		catch (UnsatisfiedLinkError e) { }
	}
*/
	
}
