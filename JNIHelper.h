#ifndef  __Admob_Helper_H_
#define  __Admob_Helper_H_

extern "C"
{
	extern void openAppJNI();
	extern void rateAppJNI();
	extern void showInterstitialJNI();
	extern void CBShowMoreApps();
    extern void showAdmobJNI();
    extern void hideAdmobJNI();
    extern int  getBannerHeightJNI();
    extern void googlePurchaseRemoveAds();
    extern bool hasRemovedAds();
    extern bool isChinaMarket();
    extern int  daysSinceInstall();

}
#endif  //__Admob_Helper_H_
